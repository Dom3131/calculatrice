import unittest

from calculatrice import calculatrice


class Test_Calculatrice(unittest.TestCase):
    
    def setUp(self):
        self.cal = calculatrice.Calculatrice()
   
    def test_additionner(self):
        self.assertEqual(2,self.cal.additionner(1,1),"l'addition des deux nombres est mauvaise")

    def test_soustraire(self):
        self.assertEqual(1,self.cal.soustraire(3,2),"la soustraction des deux nombres est mauvaise")
        
    def test_multiplier(self):
        self.assertEqual(6,self.cal.multiplier(2,3),"la multiplication des deux nombres est mauvaise")
        
    def test_racine_carree(self):
        self.assertAlmostEqual(self.cal.racine_carree(2),1.4,places=1)
        
    def test_racine_carree_exception(self):
        self.assertRaises(ValueError, self.cal.racine_carree, -1)