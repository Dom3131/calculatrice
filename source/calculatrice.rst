calculatrice package
====================

Submodules
----------

calculatrice.calculatrice module
--------------------------------

.. automodule:: calculatrice.calculatrice
   :members:
   :undoc-members:
   :show-inheritance:

calculatrice.calcule module
---------------------------

.. automodule:: calculatrice.calcule
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: calculatrice
   :members:
   :undoc-members:
   :show-inheritance:
