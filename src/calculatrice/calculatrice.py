"""
Module de démonstration d'une classe à tester
"""

import math


class Calculatrice:
    """
    Fonctions basiques d'une calculatrice sur des entiers
    """
    def additionner (self, a, b):
        """calcule la somme de 2 entiers

        Args:
            a (int): opérande 1
            b (int): opérande 2

        Returns:
            int: résultat de la somme des 2 entiers en entrée
        """

        return a+b

    def soustraire (self, a, b):
        """calcule la somme de 2 entiers

        Args:
            a (int): opérande 1
            b (int): opérande 2

        Returns:
            int: résultat de la somme des 2 entiers en entrée
        """
        return a-b


    def racine_carree (self, a):
        """calcule la racine carrée d'un entier

        Args:
            a (int): opérande 

        Returns:
            float: résultat de la racine carrée d'un entier
        """
        return math.sqrt(a)
     
    def multiplier (self,a, b):
        """calcule la multiplication de 2 entiers

        Args:
            a (int): opérande 1
            b (int): opérande 2

        Returns:
            int: résultat de la multiplication des deux entiers en entrée
        """
        return a*b

    def demo_bugs(self):
        a = b
        c = 0
        d = 1/c





