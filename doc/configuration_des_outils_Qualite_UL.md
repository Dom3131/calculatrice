# Configuration des outils Qualité Python

## Installer coverage et pytest

1. Installer coverage avec pip install:

```python
pip install --proxy=https://compte:secret@proxy-surf.loc.cnes.fr:8050  coverage

(.venv) PS C:\Users\bagotd\Documents\DEV\calculatrice> coverage --version
Coverage.py, version 6.4.1 with C extension
Full documentation is at https://coverage.readthedocs.io
``` 

2. Installer pytest  avec pip install:

``` 
pip install --proxy=https://bagotd:SeptP0ules@proxy-surf.loc.cnes.fr:8050  pytest
(.venv) PS C:\Users\bagotd\Documents\DEV\calculatrice> pytest --version
pytest 7.1.2

```

## lancer une couverture de code

Il suffit de lancer les TUs avec coverage, en utilisant pytest:


`coverage run --source=src -m pytest tests/test_calculatrice.py`

lancer la commande `report`et s'assurer que l'outil a bien balayé tous les fichiers sources, en produisant des fichiers HTML:

`coverage html -d coverage_html`