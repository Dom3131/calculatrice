# Projet de demo python "calculatrice"

## Objectif du projet 

L'objectif est de décrire les étapes d'analyse d'un code Python dans l'Usine Logicielle du CNES:
- Rédaction d'un code python simple (une classe, quelques tests unitaires...)
-  Gestion en configuration dans Gitlab
- Configuration des outils dans l'UL:
    - Gitlab
    - SonarQSube
    - Jenkins 
- Analyse du code dans SonarQube
 

 ## Sommaire 

1. [Comprendre le code](./doc/description_du_code.md)

2. [Installer et lancer les exécutable et tests unitaires](./doc/installation_et_utilisation_du_code.md)

3.  [Configurer les outils de l'Usine Logicielle du CNES](./doc/configuration_des_outils_UL.md)

4. [Configurer les outils Qualité Python et les faire tourner dans l'Usine Logicielle](./doc/configuration_des_outils_Qualite_UL.md)


